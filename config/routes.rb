Rails.application.routes.draw do
  devise_for :users

  namespace :api do
    namespace :v1 do
      resources :users, only: %i[show]
      post 'users/sign_in', to: 'users#sign_in'

      resources :mangas, only: %i[show]
      resources :chapters, only: %i[show]
    end
  end

  devise_scope :user do
    authenticated :user do
    	root to: 'searchs#index'

      resources :searchs, only: %i[index]
      resources :mangas, only: %i[index show]
      resources :chapters, only: %i[show]
      post 'images/show', to: 'images#show_image', as: :show_image

      resources :mangamarks, only: %i[create destroy]
      resources :chapter_visiteds, only: %i[create destroy]

      get 'news', to: 'dashboards#news', as: :news
      get 'api', to: 'dashboards#api', as: :api

    	post 'search', to: 'searchs#search', as: :search
    end
    unauthenticated :user do
    	root to: 'devise/sessions#new'
    end
  end
end
