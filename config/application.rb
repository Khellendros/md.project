require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MdProject
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.time_zone = "UTC"
    config.active_record.default_timezone = :utc

    db_yml = Rails.root.join("config", "database.yml")
    db_yml_example = Rails.root.join("config", "database.yml.example")

    if !File.exist?(db_yml) && File.exist?(db_yml_example)
      FileUtils.cp(db_yml_example, db_yml)
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
