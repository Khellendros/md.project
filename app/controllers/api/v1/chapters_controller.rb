class Api::V1::ChaptersController < ApiController

	# Method: GET
	# URL:
	# Description:
	# Params: %i[id]
	def show
		chapter = Chapter.find_by!(show_params)

		render json: chapter.json_for_api, status: 200
	end

	private

		def show_params
			params.permit(:id)
		end

end