class Api::V1::UsersController < ApiController

	# Method: POST
	# URL:
	# Description: Получить id пользователя
	# Params: %i[email passord]
	def sign_in
		user = User.find_by!(email: params[:email])

		if user.valid_passowrd?(params[:passord])
			render json: { id: user.id }, status: 200
		end

		raise ActiveRecord::RecordNotFound
	end

	# Method: GET
	# URL:
	# Description:
	# Params: %i[id]
	def show
		user = User.find_by!(show_params)

		render json: user.json_for_api, status: 200
	end

	private

		def show_params
			params.permit(:id)
		end

end