class Api::V1::MangasController < ApiController

	# Method: GET
	# URL:
	# Description:
	# Params: %i[id]
	def show
		manga = Manga.find_by!(show_params)

		render json: manga.json_for_api, status: 200
	end

	private

		def show_params
			params.permit(:id)
		end

end