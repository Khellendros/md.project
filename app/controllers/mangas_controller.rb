class MangasController < ApplicationController

	include ApplicationHelper

	before_action :current_path

	# Method: GET
	# URL:
	# Description: получить закладки текущего пользователя
	# Params: %i[]
	def index
		datas = current_user.mangas.as_json
		
		@mangas = []
		promises = []

		datas.each do |data|
			promises << Concurrent::Promise.execute do
				{
					"cover_base64" => image_base64(data['cover'])
				}.merge!(data)
			end
		end

		@mangas = Concurrent::Promise.zip(*promises).value!
	end

	# Method: GET
	# URL:
	# Description: получить мангу по id
	# Params: %i[id]
	def show
		@manga = Manga.find(params[:id])

		@manga.loadChapters
		@manga.reload
		
		@chapters = @manga.chapters_with_status(current_user.id)
	end

	private

		def current_path
			@manga_path = true
		end
end