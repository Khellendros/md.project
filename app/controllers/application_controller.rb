class ApplicationController < ActionController::Base

  before_action :authenticate_user!
  
  after_action do
    ActiveRecord::Base.connection_pool.disconnect!
  end

	rescue_from ActiveRecord::RecordNotFound, with: :rescue404
  rescue_from ActionController::RoutingError, with: :rescue404
  rescue_from ActionController::MissingFile, with: :rescue404

  def rescue404
    respond_to do |format|
      format.html { render file: "#{Rails.root}/public/404", layout: false, status: :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end
end
