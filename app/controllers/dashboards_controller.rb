class DashboardsController < ApplicationController

	before_action :news_path, only: %i[news]
	before_action :api_path, only: %i[api]

	def news

	end

	def api; end

	private

		def news_path
			@news_path = true
		end

		def api_path
			@api_path = true
		end
end