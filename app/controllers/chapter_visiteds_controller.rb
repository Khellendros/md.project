class ChapterVisitedsController < ApplicationController

	# Method: POST
	# URL:
	# Description: создать заметку о просмотре главы
	# Params: %i[]
	def create; end

	# Method: DELETE
	# URL:
	# Description: удалить заметку о просмотре главы
	# Params: %i[]
	def destroy; end

end