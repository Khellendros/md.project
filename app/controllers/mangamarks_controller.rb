class MangamarksController < ApplicationController

	# Method: POST
	# URL:
	# Description: Создать мангу (если нету) и добавить в закладки текущему пользователю
	# Params: %i[title cover link]
	def create
		Manga.transaction do
			mangamark = current_user.addManga(manga_params)
			redirect_to manga_path(mangamark['manga_id'])
		end
	end

	# Method: DELETE
	# URL:
	# Description: удалить мангу с id из закладок текущего пользователя
	# Params: %i[id]
	def destroy
		@manga_id = params[:id]

		mangamark = current_user.mangamarks.find_by(manga_id: @manga_id)
		mangamark.destroy!

		respond_to do |format|
      format.js
    end
	end

	private

		def manga_params
			params.require(:manga).permit(:title, :cover, :link)
		end
end