class SearchsController < ApplicationController

	require 'httparty'
	require 'uri'

	include ApplicationHelper

	before_action :current_path

	# Method: GET
	# URL:
	# Description:
	# Params: %i[]
	def index; end

	# Method: POST
	# URL:
	# Description: найти мангу по названию
	# Params: %i[title]
	def search
		hosts = JSON.parse(ENV['MANGA_HOST_URLS'])
		search_url = ENV['SEARCH_URL']
		headers = JSON.parse(ENV['HEADERS'])
		body = {
			'q': params[:title],
			' ': 'Искать!'
		}

		response = HTTParty.post(search_url, {
			headers: headers,
			body: body
		})

		html_doc = Nokogiri::HTML(response.body)
		manga_results = html_doc.at_css('div#mangaResults div.tiles')

		@mangas = []
		promises = []

    manga_results&.children.each_with_index do |children, index|
    	next if children.name != 'div'
    	next if children.attributes['class'].value != 'tile col-sm-6 '
    	next if !children.at_css('i.fa.fa-user').nil?
    	
    	title_with_link = children.at_css('h3 a')
    	link = title_with_link.attributes['href'].value

    	host = URI(link).host.to_s
    	next unless hosts.include?(host)

    	title = title_with_link.text
    	cover_div = children.at_css('img.lazy')
    	cover = cover_div.nil? ? nil : cover_div.attributes['data-original'].value
			link = "http://readmanga.me" + link if host.blank?

			promises << Concurrent::Promise.execute do
				{
					cover: cover.to_s,
					cover_base64: image_base64(cover.to_s),
					title: title.to_s,
					link: link.to_s,
					index: index
				}
			end
		end

		@mangas = Concurrent::Promise.zip(*promises).value!
	rescue StandardError => e
		@mangas = []
	ensure
		respond_to do |format|
      format.js
    end
	end

	private

		def current_path
			@search_path = true
		end
end