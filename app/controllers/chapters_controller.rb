class ChaptersController < ApplicationController

	before_action :current_path

	# Method: GET
	# URL:
	# Description: получить главу манги. Загрузить изображения, если нету. Создать запись в просмотренных
	# Params: %i[id]
	def show
		@chapter = Chapter.find(params[:id])

		@chapter.updateVisiteds(current_user.id)

		if @chapter.images.blank?
			@chapter.loadImages
			@chapter.reload
		end
	end

	private

		def current_path
			@manga_path = true
		end
end