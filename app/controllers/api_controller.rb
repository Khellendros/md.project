class ApiController < ActionController::Base
  after_action do
    ActiveRecord::Base.connection_pool.disconnect!
  end
  
  rescue_from ActiveRecord::RecordInvalid do |exception|
    render json: {
      error: exception.message,
    }, status: 400
  end

  rescue_from ActiveRecord::NotNullViolation do |exception|
    render json: {
      error: exception.message,
    }, status: 400
  end

  rescue_from ActiveRecord::RecordNotUnique do |exception|
    render json: {
      error: exception.message,
    }, status: 400
  end

  rescue_from ActionController::ParameterMissing do |exception|
    render json: {
      error: exception.message,
    }, status: 400
  end

  rescue_from ActionController::BadRequest do |exception|
    render json: {
      error: exception.message,
    }, status: 400
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: {
      error: exception.message,
    }, status: 404
  end

  rescue_from ActiveRecord::StatementInvalid do |exception|
    parsed_message = exception.message.match(/ERROR:  (.*)$/)
    render json: {
      error: parsed_message ? parsed_message[0] : exception.message,
    }, status: 400
  end
end