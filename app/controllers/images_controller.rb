class ImagesController < ApplicationController

	skip_before_action :verify_authenticity_token

	# Method: POST
	# URL:
	# Description: Отдает изображение по id
	# Params: %i[id]
	def show_image
		@image = Image.find(params[:id])

		respond_to do |format|
      format.js
    end
	end

end