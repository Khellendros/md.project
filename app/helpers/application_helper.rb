module ApplicationHelper

	require 'open-uri'
	require 'base64'

	# Description: Преобразует картинку в base64 и отображает. Это делается потому,
	#              что статика с серверов ридманги адекватно не отдается для других доменов,
	#              но нормально отдается для localhost-а.
	# Params: %i[link alt]
	def image_view_helper(link, alt = '')
		image_tag(image_base64(link), alt: alt, title: alt)
	end

	def image_base64(link)
		data = ''

		unless link.blank?
			image = open(link)
			data = 'data:image/jpeg;base64,' + Base64.strict_encode64(image.read)
		end

		data
	end

end
