class Chapter < ApplicationRecord

	require 'httparty'

	belongs_to :manga

	has_many :images, -> { order('position ASC') }, dependent: :destroy
	has_many :chapter_visiteds, dependent: :destroy

	def next_id
		res = ActiveRecord::Base.connection.exec_query("
			SELECT
				id
			FROM chapters
			WHERE manga_id = '#{self.manga_id}' 
				AND position = '#{self.position + 1}'
		").to_hash[0]

		return res ? res['id'] : nil
	end

	def prev_id
		res = ActiveRecord::Base.connection.exec_query("
			SELECT
				id
			FROM chapters
			WHERE manga_id = '#{self.manga_id}' 
				AND position = '#{self.position - 1}'
		").to_hash[0]

		return res ? res['id'] : nil
	end

	def json_for_api
		self.as_json(
			only: %i[id title position manga_id],
			include: {
				images: {
					only: %i[link position]
				}
			}
		)
	end

	# Description: Создает запись о просмотре юзером главы
	# Params: %i[user_id]
	def updateVisiteds(user_id)
		sql_query = <<-SQL
			INSERT INTO chapter_visiteds (chapter_id, user_id, created_at, updated_at)
			VALUES ('#{self.id}', '#{user_id}', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
			ON CONFLICT (chapter_id, user_id) DO UPDATE
			SET updated_at = CURRENT_TIMESTAMP
		SQL
		ActiveRecord::Base.connection.exec_query(sql_query)
	end

	# Description: Загрузить и сохранить слайды для текущей главы
	# Params %i[]
	def loadImages
		headers = JSON.parse(ENV['HEADERS'])

		response = HTTParty.get(self.link + "?mtr=1", {
    	headers: headers
    })

    _images = []

    text_with_images_links = /rm_h.init\((.+)\)/.match(response.body)[0]
    text_with_images_links = /\[(.+)\]/.match(text_with_images_links)[0]
    array_with_images_links = eval(text_with_images_links)

	  array_with_images_links.each_with_index do |image_link|
	  	next if image_link.nil?

	    _link = image_link[1] + image_link[2]
	    _images.push(link: _link)
		end
		
		return if _images.blank?

	  self.transaction do
	  	sql_query = <<-SQL
				INSERT INTO images (position, link, chapter_id, created_at, updated_at)
					SELECT 
						pos,
						elem ->> 'link',
						'#{self.id}',
						CURRENT_TIMESTAMP,
						CURRENT_TIMESTAMP 
					FROM jsonb_array_elements(?) WITH ORDINALITY arr(elem, pos)
				ON CONFLICT (chapter_id, position) DO UPDATE
				SET updated_at = CURRENT_TIMESTAMP
			SQL
			ActiveRecord::Base.connection.execute(
				ActiveRecord::Base.send(
					:sanitize_sql_array, [
						sql_query,
						_images.to_json
					]
				)
			)
	  end
	rescue StandardError => e
		Rails.logger.error e
		Rails.logger.error e.backtrace
	ensure
		return
	end

end
