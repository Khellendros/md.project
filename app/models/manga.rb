class Manga < ApplicationRecord

	has_many :chapters, -> { order('position DESC') }, dependent: :destroy
	has_many :mangamarks, dependent: :destroy

	def json_for_api
		self.as_json(
			only: %i[id title cover],
			include: {
				chapters: {
					only: %i[id title position]
				}
			}
		)
	end

	# Description: Получить главы для текущей манги со статусами просмотра юзера
	# Params %i[user_id]
	def chapters_with_status(user_id)
		query = <<-SQL
			SELECT
				chapters.id AS id, 
				chapters.title AS title, 
				chapters.position AS position,
				CASE WHEN chapter_visiteds.id IS NULL THEN false ELSE true END AS status
			FROM chapters
			LEFT JOIN chapter_visiteds ON chapter_visiteds.chapter_id = chapters.id AND chapter_visiteds.user_id = '#{user_id}'
			WHERE chapters.manga_id = '#{self.id}'
			ORDER BY chapters.position DESC
		SQL
		ActiveRecord::Base.connection.exec_query(query).to_hash
	end

	# Description: Загрузить и сохранить главы для текущей манги
	# Params %i[]
	def loadChapters
		manga_host = URI(self.link).host.to_s

		hosts = JSON.parse(ENV['MANGA_HOST_URLS'])
		headers = JSON.parse(ENV['HEADERS'])

		response = HTTParty.get(self.link, {
    	headers: headers
    })

		html_doc = Nokogiri::HTML(response.body)

		chapter_results = html_doc.at_css('div.chapters-link table')

		_chapters = []

		(chapter_results&.children || []).each do |children|

			next if children.name != 'tr'

			children.children.each do |children_children|
				next if children_children.name != 'td'

				link_with_title = children_children.at_css('a')

				next if link_with_title.nil?

				_link = link_with_title['href']
				host = URI(_link).host.to_s

				next unless hosts.include?(host)

				_title = link_with_title.text.split(' ').join(' ')
				_title.slice!('новое')
				
				_link = "http://" + manga_host + _link if host.blank?

				_chapters.push(title: _title, link: _link)						
			end	
		end

		return if _chapters.blank?

		self.transaction do
			sql_query = <<-SQL
				INSERT INTO chapters (link, title, position, manga_id, created_at, updated_at)
					SELECT 
						elem ->> 'link',
						elem ->> 'title',
						pos,
						'#{self.id}',
						CURRENT_TIMESTAMP,
						CURRENT_TIMESTAMP 
					FROM jsonb_array_elements(?) WITH ORDINALITY arr(elem, pos)
				ON CONFLICT (manga_id, position) DO UPDATE
				SET updated_at = CURRENT_TIMESTAMP,
						title = EXCLUDED.title
			SQL
			ActiveRecord::Base.connection.execute(
				ActiveRecord::Base.send(
					:sanitize_sql_array, [
						sql_query,
						_chapters.reverse.to_json
					]
				)
			)
		end
	rescue StandardError => e
		Rails.logger.error e
		Rails.logger.error e.backtrace
	ensure
		return
	end
end
