class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, 
         :registerable, # регистрация
         # :recoverable, # восстановление пароля
         # :confirmable, # подтверждение email'a
         # :lockable, # блокировка аккаунта
         :rememberable, # запомнить пользователя
         :validatable # проверка

  has_many :mangamarks, dependent: :destroy
  has_many :chapter_visiteds, dependent: :destroy

  has_many :mangas, through: :mangamarks, source: :manga, class_name: 'Manga'

  def json_for_api
  	self.as_json(
  		only: %i[id], 
  		include: {
  			mangamarks: {
  				only: %i[id],
          include: {
            manga: {
              only: %i[id title cover]
            }
          }
  			}
  		}
  	)
  end

  # Description: Создает мангу и добавляет её в закладки. Возвращает manga_id
  # Params: %i[manga_params] // manga_params = { title: string, cover: string, link: string }
  def addManga(manga_params)
    sql_query = <<-SQL
      WITH created_mangas AS (
        INSERT INTO mangas (title, cover, link, created_at, updated_at)
        VALUES (
          '#{manga_params['title']}',
          '#{manga_params['cover']}',
          '#{manga_params['link']}', 
          CURRENT_TIMESTAMP,
          CURRENT_TIMESTAMP
        )
        ON CONFLICT (link) DO UPDATE
        SET updated_at = CURRENT_TIMESTAMP
        RETURNING *
      )
      INSERT INTO mangamarks (manga_id, user_id, created_at, updated_at)
        SELECT id, '#{self.id}', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM created_mangas
      ON CONFLICT (manga_id, user_id) DO UPDATE
      SET updated_at = CURRENT_TIMESTAMP
      RETURNING manga_id
    SQL
    ActiveRecord::Base.connection.exec_query(sql_query).to_hash[0]
  end  
end
