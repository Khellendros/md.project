class CreateMangas < ActiveRecord::Migration[5.2]
  def change
    create_table :mangas, id: :uuid do |t|

    	t.string :title, null: false, default: ''
    	t.string :cover
    	t.string :link, null: false, default: ''

    	t.index :link, unique: true
    	t.index :title

      t.timestamps
    end
  end
end
