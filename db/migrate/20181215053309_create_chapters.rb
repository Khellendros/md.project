class CreateChapters < ActiveRecord::Migration[5.2]
  def change
    create_table :chapters, id: :uuid do |t|

    	t.string :title, null: false, default: ''
    	t.integer :position, null: false, default: 1
    	t.string :link, null: false, default: ''

    	t.references :manga, index: true, type: :uuid

    	t.index :link, unique: true
    	t.index :title
    	t.index %w[manga_id position], unique: true, name: 'index_chapters_on_manga_id_and_position'

      t.timestamps
    end
  end
end
