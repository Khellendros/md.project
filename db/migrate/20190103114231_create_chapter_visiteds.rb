class CreateChapterVisiteds < ActiveRecord::Migration[5.2]
  def change
    create_table :chapter_visiteds, id: :uuid do |t|

    	t.references :user, index: true, type: :uuid
    	t.references :chapter, index: true, type: :uuid

      t.timestamps

      t.index %w[user_id chapter_id], unique: true, name: 'index_visiteds_on_user_id_and_chapter_id'
    end
  end
end
