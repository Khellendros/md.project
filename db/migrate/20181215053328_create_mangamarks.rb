class CreateMangamarks < ActiveRecord::Migration[5.2]
  def change
    create_table :mangamarks, id: :uuid do |t|

    	t.references :user, index: true, type: :uuid
    	t.references :manga, index: true, type: :uuid

      t.timestamps

      t.index %w[user_id manga_id], unique: true, name: 'index_mangamarks_on_user_id_and_mangd_id'
    end
  end
end
