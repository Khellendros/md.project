class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images, id: :uuid do |t|

    	t.string :link, null: false, default: ''
    	t.integer :position, null: false, default: 1

    	t.references :chapter, index: true, type: :uuid

    	t.index :link, unique: true
    	t.index %w[chapter_id position], unique: true, name: 'index_images_on_chapter_id_and_position'

      t.timestamps
    end
  end
end
