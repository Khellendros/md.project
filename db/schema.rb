# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_03_114231) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "chapter_visiteds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "chapter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chapter_id"], name: "index_chapter_visiteds_on_chapter_id"
    t.index ["user_id", "chapter_id"], name: "index_visiteds_on_user_id_and_chapter_id", unique: true
    t.index ["user_id"], name: "index_chapter_visiteds_on_user_id"
  end

  create_table "chapters", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", default: "", null: false
    t.integer "position", default: 1, null: false
    t.string "link", default: "", null: false
    t.uuid "manga_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["link"], name: "index_chapters_on_link", unique: true
    t.index ["manga_id", "position"], name: "index_chapters_on_manga_id_and_position", unique: true
    t.index ["manga_id"], name: "index_chapters_on_manga_id"
    t.index ["title"], name: "index_chapters_on_title"
  end

  create_table "images", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "link", default: "", null: false
    t.integer "position", default: 1, null: false
    t.uuid "chapter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chapter_id", "position"], name: "index_images_on_chapter_id_and_position", unique: true
    t.index ["chapter_id"], name: "index_images_on_chapter_id"
    t.index ["link"], name: "index_images_on_link", unique: true
  end

  create_table "mangamarks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "manga_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["manga_id"], name: "index_mangamarks_on_manga_id"
    t.index ["user_id", "manga_id"], name: "index_mangamarks_on_user_id_and_mangd_id", unique: true
    t.index ["user_id"], name: "index_mangamarks_on_user_id"
  end

  create_table "mangas", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", default: "", null: false
    t.string "cover"
    t.string "link", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["link"], name: "index_mangas_on_link", unique: true
    t.index ["title"], name: "index_mangas_on_title"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
